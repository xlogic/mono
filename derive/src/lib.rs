// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

extern crate proc_macro;
use proc_macro::TokenStream;

#[proc_macro_derive(HelperAttr, attributes(helper))]
pub fn derive_helper_attr(item: TokenStream) -> TokenStream {
    println!("my macro! {} {}", item.to_string(), TokenStream::new().to_string());

    TokenStream::new()
}
