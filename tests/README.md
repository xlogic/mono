# xlogic-tests

[[_TOC_]]

## Description

The mono repository for **xlogic** standalone libraries, services and command line tools.

## Development

To verify it:

```plaintext
cargo check
```

To run tests:

```plaintext
cargo test
```
