// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

extern crate xlogic_derive;

#[derive(xlogic_derive::HelperAttr)]
pub struct Struct {
    #[helper] field: i32,
    field2: i64,
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let mut variable = super::Struct {
            field: 5,
            field2: 1,
        };

        variable.field = 10;
    }
}
